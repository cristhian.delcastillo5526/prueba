import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

@Injectable({providedIn:'root'})

export class loginGuard implements CanActivate{

  constructor(private router:Router) { }

  guardarLogin: Storage = sessionStorage;
  canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot){

    if(this.guardarLogin.getItem("login")=="true")
    {
        return true;
    }
    else{

      this.router.navigate(["/login"]);
      return false;
    }

  }


}
