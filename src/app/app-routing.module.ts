import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearProductoComponent } from './crear-producto/crear-producto.component';
import { EditarProductComponent } from './editar-product/editar-product.component';
import { loginGuard } from './guards/login.guard';
import { HomeComponent } from './home/home.component';
import { IndividualProductComponent } from './individual-product/individual-product.component';
import { LoginComponent } from './login/login.component';
import { AdminProductsComponent } from './admin-products/admin-products.component';
import { ComprasComponent } from './compras/compras.component';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';
import { AdminUsuariosComponent } from './admin-usuarios/admin-usuarios.component';


const routes: Routes = [
  {path:'product/:id',component:IndividualProductComponent},
  {path:'admin-products/type/:typeProduct',component:AdminProductsComponent},
  {path:'product-create',component:CrearProductoComponent},
  {path: 'home', component:HomeComponent },
  {path: 'login', component:LoginComponent},
  {path: 'product-edit/:id',component:EditarProductComponent},
  {path: '',redirectTo:"/home",pathMatch:'full'},
  {path: 'admin-products',component:AdminProductsComponent},
  {path: 'compras', component:ComprasComponent},
  {path:  'admin-usuarios', component:AdminUsuariosComponent},
  {path: 'crear-usuario', component:CrearUsuarioComponent},
  {path: 'admin-usuarios/:usuarioId',component:EditarUsuarioComponent},

  /*canActivate:[loginGuard]   <---------llave del guardian */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
