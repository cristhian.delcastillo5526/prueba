import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { comprasRequest } from '../models/comprasRequest.model';
import { CreateProductRequest } from '../models/createProductRequest.model';
import { ComprasService } from '../services/Compras/compras.service';
import { ProductosService } from '../services/products/productos.service';


@Component({
  selector: 'app-individual-product',
  templateUrl: './individual-product.component.html',
  styleUrls: ['./individual-product.component.css']
})
export class IndividualProductComponent implements OnInit {

  individual = [];
  displayedColumns: string[] = [
    'id',
    'name',
    'precio',
    'weight',
    'status',
    'typeProduct',
    'comprarproducto',
  ];

  usuario: any;
  storage: Storage = sessionStorage;

  producto: any = {};
  id: number;
  imageUrl: string;
  isAdmin: boolean = false;
  venta: any = {};


  constructor(private product: ProductosService, private route: ActivatedRoute, private compra:ComprasService) { }
  ngOnInit(): void {

    /****verificar el login */
    const login = this.storage.getItem('login');
    console.log('login', login);
    if (login) {
      this.usuario = JSON.parse(login);
      this.isAdmin = this.usuario.administrador;
    }

    this.id = this.route.snapshot.params.id;
    this.product.individualProduct(this.id).subscribe(data => {

      if (data == null) {

        this.producto = [];
      }

      else {
        this.producto = [data];
        this.imageUrl =  data.imagen==null ? null :'https://localhost:44337/product/imagen/' + data.imagen; //esto es un operador ternario es como in if else en una sola linea
        //imprimir en consola
       // console.log('Productos', data);
      }
    });
  }

  onComprar(event: number){


        let request: comprasRequest={...this.producto[0]};
        request.usuarioId=this.usuario.usuarioId;
        this.compra.comprarProducto(request).subscribe(data=>{

          alert("Producto Agregado");
   });
  }





}
