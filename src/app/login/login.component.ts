import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { _MatDialogContainerBase } from '@angular/material/dialog';
import { Data, Router } from '@angular/router';
import { equal } from 'assert';
import { loginRequest } from '../models/loginRequest.model';
import { LoginService } from '../services/login/login.service';
import { ProductosService } from '../services/products/productos.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  listaUsuarios = [];
  displayedColumns: string[] = [
    'id',
    'Usuario',
    'Contrasena',
    'Administrador',
  ];

  formularioLogin: FormGroup;
  productoFormulario: any;
  guardarLogin: Storage = sessionStorage;
  isLogged = false;
  constructor(private loginService:LoginService, private router:Router) { }

  ngOnInit(): void {

    this.formularioLogin = new FormGroup({

      Correo: new FormControl(null),
      Contrasena: new FormControl(null)

    });

    const login = this.guardarLogin.getItem("login");

    if(login) {
      this.isLogged = true;
    }
  }



  onLogin():void{

    let request: loginRequest={...this.formularioLogin.value};

    this.loginService.login(request).subscribe(data=>{
      if( data !== null && data !== undefined ) {
        this.guardarLogin.setItem("login", JSON.stringify(data));
        this.isLogged = true;
        this.router.navigate(['/admin-products']);
      }
      else{
        alert('Correo o Contraseña Invalidos');
      }

    });



  }

  onLogout(): void {
    this.isLogged = false;
    sessionStorage.clear();
    this.router.navigate(['/home']);
  }

}
