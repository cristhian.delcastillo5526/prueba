import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EditeUsuarioRequest } from '../models/editeUsuarioRequest.model';
import { UsuariosService } from '../services/usuarios.service';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {

    usuarioFormulario: FormGroup;

    constructor(private usuarioService: UsuariosService, private router:Router, private routing:ActivatedRoute) { }
    usuarioId: number;
    ngOnInit(): void {

      this.usuarioFormulario = new FormGroup({

        correo: new FormControl(null),
        contrasena: new FormControl(null),
        nombre: new FormControl(null),
        domicilio: new FormControl(null),
        telefono: new FormControl(null),
        administrador: new FormControl(null)

      });

      this.usuarioService.individualUsuario(this.usuarioId).subscribe(data => {

        if (data == null) {

          alert("Usuario no registrado");

        }

        else {

          this.usuarioFormulario.patchValue(data);
          if(data.status){

            this.usuarioFormulario.controls.status.setValue('true');

          }
          else{
            this.usuarioFormulario.controls.status.setValue('false');
          }

        }


      });

    }

    onGuardar():void{

      let request: EditeUsuarioRequest={...this.usuarioFormulario.value};

      request.Administrador=this.usuarioFormulario.controls.Administrador.value=='true';

      this.usuarioService.actualizarUsuario(request,this.usuarioId).subscribe(data=>{

        alert("Usuario Guardado");
        this.router.navigate(['/usuarios']);
      });
    }

}
