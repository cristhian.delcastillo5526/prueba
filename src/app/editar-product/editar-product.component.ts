import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EditeProductRequest } from '../models/editeProductRequest.models';
import { ProductosService } from '../services/products/productos.service';

@Component({
  selector: 'app-editar-product',
  templateUrl: './editar-product.component.html',
  styleUrls: ['./editar-product.component.css']
})
export class EditarProductComponent implements OnInit {

  productoFormulario: FormGroup;

  constructor(private productService: ProductosService, private router:Router, private routing:ActivatedRoute) { }
  productId: number;
  ngOnInit(): void {

    this.productId = this.routing.snapshot.params.id;

    this.productoFormulario = new FormGroup({

      name: new FormControl(null),
      precio: new FormControl(null),
      weight: new FormControl(null),
      status: new FormControl(null),
      typeProduct: new FormControl(null)

    });

    this.productService.individualProduct(this.productId).subscribe(data => {

      if (data == null) {

        alert("Producto no registrado");

      }

      else {

        this.productoFormulario.patchValue(data);
        if(data.status){

          this.productoFormulario.controls.status.setValue('true');

        }
        else{
          this.productoFormulario.controls.status.setValue('false');
        }

      }


    });

  }

  onGuardar():void{

    let request: EditeProductRequest={...this.productoFormulario.value};

    request.status=this.productoFormulario.controls.status.value=='true';

    this.productService.actualizarProducto(request,this.productId).subscribe(data=>{

      alert("Producto Guardado");
      this.router.navigate(['/admin-products']);
    });
  }

}
