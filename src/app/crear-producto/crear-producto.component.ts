import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductosService } from '../services/products/productos.service';
import {CreateProductRequest} from '../models/createProductRequest.model';

@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.css']
})
export class CrearProductoComponent implements OnInit {

  productoFormulario: FormGroup;
  constructor(private productoService:ProductosService, private router:Router) { }

  ngOnInit(): void {

    this.productoFormulario = new FormGroup({

      name: new FormControl(null),
      precio: new FormControl(null),
      weight: new FormControl(null),
      status: new FormControl(null),
      typeProduct: new FormControl(null),
      Imagen: new FormControl(null)

    });
  }

  onGuardar():void{

    let request: CreateProductRequest={...this.productoFormulario.value};

    request.status=this.productoFormulario.controls.status.value=='true';

    this.productoService.crearProducto(request).subscribe(data=>{



      let formData: FormData = new FormData();
       formData.append('file', this.fileToLoad, this.fileToLoad.name);

       this.productoService.guardarImagen(formData, data.id).subscribe();

       alert("Producto Guardado");

       this.router.navigate(['/admin-products']); //verificar


    });
  }

  fileToLoad:File;
  saveFile(event:any){

    this.fileToLoad = event.target.files[0];

  }
}
