import { typeofExpr } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuariosService } from '../services/usuarios.service';

@Component({
  selector: 'app-admin-usuarios',
  templateUrl: './admin-usuarios.component.html',
  styleUrls: ['./admin-usuarios.component.css']
})

export class AdminUsuariosComponent implements OnInit {

  lista = [];
  displayedColumns: string[] = [
    'usuarioId',
    'Nombre',
    'Correo',
    'Domicilio',
    'Administrador',
    'Telefono',
    'editarausuario',
    'verusuario',
    'eliminarusuario',
  ];
  usuario: any;
  isAdmin: boolean = false;
  storage: Storage = sessionStorage;

  producto: any = {};
  typeProduct: number;


  constructor(private product: UsuariosService, private route: ActivatedRoute, private router:Router) {}

  ngOnInit(): void {

   this.actualizar();

    const login = this.storage.getItem('login');
    console.log('login', login);
    if (login) {
      this.usuario = JSON.parse(login);
      this.isAdmin = this.usuario.administrador;
    }

  }


  onDelete(event: number): void {
    this.usuario.deleteUsuario(event).subscribe((data) => {

      alert('Usuario Eliminado');
        this.allUsuarios();
    });
  }

  private actualizar(): void {
    this.product.listUsuarios().subscribe(data => {

      if (data == null || data.length == 0) {

        this.lista = [];
        alert("Por el momento no hay productos registrados, Por favor intentelo mas tarde");

      }

      else {


        this.lista = data;

      }

  });
  }



  private allUsuarios(): void{

    this.usuario.listUsuarios().subscribe(data => {

      this.lista = data;

    });
  }

}
