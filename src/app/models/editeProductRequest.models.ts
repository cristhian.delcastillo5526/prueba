export interface EditeProductRequest {
    name: string, 
    precio: number, 
    weight: number, 
    status: boolean
}