export interface CreateProductRequest {
    name: string,
    precio: number,
    weight: number,
    status: boolean
}
