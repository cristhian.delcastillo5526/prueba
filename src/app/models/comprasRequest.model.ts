export interface comprasRequest {
  buyId: number,
  nameClient: string,
  directionClient: string,
  nameProduct: string,
  priceProduct: number,
  total: number,
  usuarioId: number
}
