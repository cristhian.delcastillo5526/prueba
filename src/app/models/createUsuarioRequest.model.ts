export interface CreateUsuarioRequest {
  Nombre: string,
  Correo: string,
  Contrasena: string,
  Domcilio: string,
  Telefono: number,
  Administrador: boolean
}
