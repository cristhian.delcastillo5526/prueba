import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateUsuarioRequest } from '../models/createUsuarioRequest.model';
import { UsuariosService } from '../services/usuarios.service';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {


  usuarioFormulario: FormGroup;
  constructor(private usuariosService:UsuariosService, private router:Router) { }

  ngOnInit(): void {
    this.usuarioFormulario = new FormGroup({

      Correo: new FormControl(null),
      Contrasena: new FormControl(null),
      Nombre: new FormControl(null),
      Domicilio: new FormControl(null),
      Telefono: new FormControl(null),
      Administrador: new FormControl(null)

    });
  }

  onGuardar():void{

    let request: CreateUsuarioRequest={...this.usuarioFormulario.value};

    request.Administrador=this.usuarioFormulario.controls.Administrador.value=='true';

    this.usuariosService.crearUsuario(request).subscribe(data=>{


       alert("Usuario Guardado")

       this.router.navigate(['/home']); //verificar
    });
  }
}
