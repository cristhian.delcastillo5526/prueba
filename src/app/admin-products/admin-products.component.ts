import { typeofExpr } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductosService } from '../services/products/productos.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css'],
})
export class AdminProductsComponent implements OnInit {
  lista = [];
  displayedColumns: string[] = [
    'id',
    'name',
    'precio',
    'weight',
    'status',
    'typeProduct',
    'verproducto',
    'editarproducto',
    'eliminarproducto',
  ];
  usuario: any;
  isAdmin: boolean = false;
  storage: Storage = sessionStorage;

  producto: any = {};
  typeProduct: number;


  constructor(private product: ProductosService, private route: ActivatedRoute, private router:Router) {}

  ngOnInit(): void {


    this.typeProduct = this.route.snapshot.params.typeProduct;

    if(this.typeProduct != null){

      this.actualizar();


    }

    else{

      this.allProducts();

    }


    const login = this.storage.getItem('login');
    console.log('login', login);
    if (login) {
      this.usuario = JSON.parse(login);
      this.isAdmin = this.usuario.administrador;
    }

  }

  onComprar(): void{

  }

  onDelete(event: number): void {
    this.product.deleteProduct(event).subscribe((data) => {
      alert('Producto Eliminado');


      if(this.typeProduct != null){

        this.actualizar();


      }

      else{

        this.allProducts();

      }

    });
  }

  private actualizar(): void {
    this.product.typeProductList(this.typeProduct).subscribe(data => {

      if (data == null || data.length == 0) {

        this.lista = [];
        alert("Por el momento no hay productos registrados, Por favor intentelo mas tarde");

      }

      else {


        this.lista = data;

      }

  });
  }


  private allProducts(): void{

    this.product.list().subscribe(data => {

      this.lista = data;

    });

  }


}
