import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { IndividualProductComponent } from './individual-product/individual-product.component';
import { CrearProductoComponent } from './crear-producto/crear-producto.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditarProductComponent } from '../app/editar-product/editar-product.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidenavComponent } from './sidenav/sidenav.component';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AdminProductsComponent} from './admin-products/admin-products.component';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';
import { AdminUsuariosComponent } from './admin-usuarios/admin-usuarios.component';

@NgModule({
  declarations: [
    AppComponent,
    IndividualProductComponent,
    CrearProductoComponent,
    EditarProductComponent,
    SidenavComponent,
    LoginComponent,
    HomeComponent,
    AdminProductsComponent,
    CrearUsuarioComponent,
    AdminUsuariosComponent,
    EditarUsuarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule
  ],
  entryComponents: [SidenavComponent],
  exports: [SidenavComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


/*
ERROR in node_modules/@angular/flex-layout/typings/module.d.ts:16:22
- error NG6002: Appears in the NgModule.imports of AppModule, but could not
be resolved to an NgModule class.

    This likely means that the library (@angular/flex-layout)
    which declares FlexLayoutModule has not been processed correctly by ngcc,
    or is not compatible with Angular Ivy. Check if a newer version of the library
     is available, and update if so. Also consider checking with the library's authors
     to see if the library is expected to be compatible with Ivy.
npm install @angular/flex-layout@latest --save

 comando para actualizar flex layout*/
