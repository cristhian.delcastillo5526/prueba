import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { loginRequest } from 'src/app/models/loginRequest.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }

  login(login:loginRequest):Observable<any>{

    return this.http.post('https://localhost:44337/usuario/login',login);

  }


  loginAdmin(login:loginRequest):Observable<any>{

    return this.http.post('https://localhost:44337/usuario/loginAdmin',login);
  }

}
