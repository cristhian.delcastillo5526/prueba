import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { comprasRequest } from 'src/app/models/comprasRequest.model';

@Injectable({
  providedIn: 'root'
})
export class ComprasService {

  constructor(private http:HttpClient) { }

  comprarProducto(compra:any):Observable<any>{

    return this.http.post('https://localhost:44337/compras',compra);
  }


}
