import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import {CreateProductRequest} from '../../models/createProductRequest.model';
import { EditeProductRequest } from 'src/app/models/editeProductRequest.models';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(private http:HttpClient) { }

  list():Observable<any>{
    return this.http.get('https://localhost:44337/product');

  }

  typeProductList(typeProduct:number):Observable<any>{
    return this.http.get('https://localhost:44337/product/type/'+typeProduct);

  }


  individualProduct(id:number):Observable<any>{

    return this.http.get('https://localhost:44337/product/'+id);
  }

  comprarProduct(compra:any):Observable<any>{
    return this.http.post('https://localhost:44337/compras/',compra);
  }

  deleteProduct(id:number):Observable<any>{

    return this.http.delete('https://localhost:44337/product/'+id);
  }

  crearProducto(producto:any):Observable<any>{

    return this.http.post('https://localhost:44337/product',producto);

  }


 // realizar formulario para actualizar
  actualizarProducto(producto:EditeProductRequest, id:number):Observable<any>{
    return this.http.put('https://localhost:44337/product/'+id, producto);
  }

  guardarImagen(formData:FormData, id:number):Observable<any>{

         return this.http.put('https://localhost:44337/product/put-Imagen/'+id, formData);

  }
}


