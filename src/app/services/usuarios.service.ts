import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EditeUsuarioRequest } from '../models/editeUsuarioRequest.model';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private http:HttpClient) { }

  crearUsuario(usuario:any):Observable<any>{

    return this.http.post('https://localhost:44337/usuario',usuario);
  }


  individualUsuario(UsuarioId:number):Observable<any>{

    return this.http.get('https://localhost:44337/usuario/'+UsuarioId);
  }
  actualizarUsuario(usuario:EditeUsuarioRequest, UsuarioId:number):Observable<any>{
    return this.http.put('https://localhost:44337/usuario/'+UsuarioId, usuario);
  }

  deleteUsuario(usuarioId:number):Observable<any> {

    return this.http.delete('https://localhost:44337/usuario/'+usuarioId);
  }

  listUsuarios():Observable<any>{
    return this.http.get('https://localhost:44337/usuario');

  }
}
